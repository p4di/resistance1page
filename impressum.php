<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    .imp{
        padding: 20px;
    }
</style>
</head>
<body>


    <div class="imp">
        <h1>Impressum</h1>
        <p>Angaben gemäß § 5 TMG:
            <br>
            Patrick Diepold
            <br>
            Kernerstr. 19
            <br>
        74343 Sachsenheim</p>
        
        <p>E-Mail:<a href="mailto:padi@kellox-dart.de">padi@kellox-dart.de</a></p>
        <p>Quelle: <i><a rel="nofollow" href="http://www.e-recht24.de">http://www.e-recht24.de</a></i></p>
        <div>
        <h2>Haftungsausschluss:</h2>
        <p><strong>Haftung für Inhalte</strong></p>
        <p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>
        <p><strong>Haftung für Links</strong></p>
        <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
        <p><strong>Urheberrecht</strong></p>
        <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
        <p> </p>
        <p><i>Quellen: <a rel="nofollow" href="http://www.e-recht24.de/muster-disclaimer.htm" target="_blank">Disclaimer</a> von eRecht24, dem Portal zum Internetrecht von Rechtsanwalt Sören Siebert</i></p>
        <h3>Datenschutzerklärung:</h3>
        <p><strong>Datenschutz</strong></p>
        <p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben. </p>
        <p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich. </p>
        <p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p>
        <p> </p>
        <p><strong>Datenschutzerklärung für die Nutzung von Google Analytics</strong></p>
        <p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt.</p>
        <p>Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.</p>
        <p>Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: <a href="http://tools.google.com/dlpage/gaoptout?hl=de">http://tools.google.com/dlpage/gaoptout?hl=de</a>.
        <p><strong>Cookies</strong> </p>
        <p>Cookies sind Textdateien, die automatisch bei dem Aufruf einer Webseite lokal im Browser des Besuchers abgelegt werden. Diese Website setzt Cookies ein, um das Angebot nutzerfreundlich und funktionaler zu gestalten. Dank dieser Dateien ist es beispielsweise möglich, auf individuelle Interessen abgestimmte Informationen auf einer Seite anzuzeigen. Auch Sicherheitsrelevante Funktionen zum Schutz Ihrer Privatsphäre werden durch den Einsatz von Cookies ermöglicht. Der ausschließliche Zweck besteht also darin, unser Angebot Ihren Kundenwünschen bestmöglich anzupassen und die Seiten-Nutzung so komfortabel wie möglich zu gestalten. Mit Anwendung der DSGVO 2018 sind Webmaster dazu verpflichtet, der unter https://eu-datenschutz.org/ veröffentlichten Grundverordnung Folge zu leisten und seine Nutzer entsprechend über die Erfassung und Auswertung von Daten in Kenntnis zu setzen. Die Rechtmäßigkeit der Verarbeitung ist in Kapitel 2, Artikel 6 der DSGVO begründet.</p>
        <!-- <p><strong>Google Analytics</strong></p>

        <p>Dieses Angebot nutzt ebenfalls den Webanalysedienst Google Analytics, ein Programm der Google Inc. („Google, USA“). Die durch das Tracking erfassten Informationen zu Ihrer Nutzung dieser Website werden auf einem Server von Google in den USA gespeichert. Durch eine sogenannte IP-Anonymisierung wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Somit ist der Standort Ihres Browsers lediglich regional zuortbar, nicht aber ihrer Person. Google kann Besucherverhalten auswerten, um Reports über die Websiteaktivitäten zusammenzustellen. Auch weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen können so gegenüber dem Websitebetreiber erbracht werden.</p> -->
        <p> </p>
        <p><strong>Datenschutzerklärung für die Nutzung von Google +1</strong></p>
        <p><i>Erfassung und Weitergabe von Informationen:</i><br />
            Mithilfe der Google +1-Schaltfläche können Sie Informationen weltweit veröffentlichen. über die Google +1-Schaltfläche erhalten Sie und andere Nutzer personalisierte Inhalte von Google und unseren Partnern. Google speichert sowohl die Information, dass Sie für einen Inhalt +1 gegeben haben, als auch Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben. Ihre +1 können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in Suchergebnissen oder in Ihrem Google-Profil, oder an anderen Stellen auf Websites und Anzeigen im Internet eingeblendet werden.<br />
            Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die Google-Dienste für Sie und andere zu verbessern. Um die Google +1-Schaltfläche verwenden zu können, benötigen Sie ein weltweit sichtbares, öffentliches Google-Profil, das zumindest den für das Profil gewählten Namen enthalten muss. Dieser Name wird in allen Google-Diensten verwendet. In manchen Fällen kann dieser Name auch einen anderen Namen ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto verwendet haben. Die Identität Ihres Google-Profils kann Nutzern angezeigt werden, die Ihre E-Mail-Adresse kennen oder über andere identifizierende Informationen von Ihnen verfügen.<br />
            <br />
            <i>Verwendung der erfassten Informationen:</i><br />
            Neben den oben erläuterten Verwendungszwecken werden die von Ihnen bereitgestellten Informationen gemäß den geltenden Google-Datenschutzbestimmungen genutzt. Google veröffentlicht möglicherweise zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer bzw. gibt diese an Nutzer und Partner weiter, wie etwa Publisher, Inserenten oder verbundene Websites. </p>
        <p> </p>
        <p><i>Quellen: <a rel="nofollow" href="http://www.e-recht24.de/muster-datenschutzerklaerung.htm" target="_blank">Datenschutzerklärung</a> von eRecht24, dem Portal zum Internetrecht von Rechtsanwalt Sören Siebert, <a rel="nofollow" href="http://www.google.com/intl/de/analytics/privacyoverview.html" target="_blank">Google Analytics Datenschutzerklärung</a>, <a rel="nofollow" href="http://www.google.com/intl/de/+/policy/+1button.html" target="_blank">Datenschutzerklärung Google +1</a></i></p>
        </p>
        </div>
    </div>
</body>
